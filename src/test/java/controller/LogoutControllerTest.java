package controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.test.context.support.WithMockUser;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WithMockUser(username = "admin", roles = "ADMIN")
public class LogoutControllerTest {

    @Mock
    Authentication authentication;
    @Mock
    SecurityContext securityContext;


    @Before
    public void testInitialization() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void logoutTest() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
    }

    @Test(expected = Exception.class)
    public void logoutNegativeTest() {
        doReturn(authentication).when(securityContext).getAuthentication().equals(null);
    }
}
