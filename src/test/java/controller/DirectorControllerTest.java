package controller;

import films.dao.DirectorDAO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DirectorControllerTest {

    @Mock
    DirectorDAO directorDAO;

    @Before
    public void testInitialization() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void updateDirectorTest() {
        doNothing().when(directorDAO).updateDirectorsFirstNameAndLastName(anyString(), anyString());
    }

    @Test(expected = Exception.class)
    public void updateDirectorNegativeTest() {
        doThrow(Exception.class)
                .when(directorDAO).updateDirectorsFirstNameAndLastName(null, "");
    }
}
