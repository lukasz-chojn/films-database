package controller;

import films.dao.DirectorDAO;
import films.dao.FilmDAO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FilmOperationControllerTest {

    @Mock
    FilmDAO filmDAO;
    @Mock
    DirectorDAO directorDAO;

    @Before
    public void testInitialization() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void deteleteExistingFilmTest() {
        doNothing().when(filmDAO).deleteFilm(anyString());
        doNothing().when(directorDAO).deleteDirector(anyString());
    }

    @Test(expected = Exception.class)
    public void deteleteExistingFilmNegativeTest() throws NoSuchMethodException {
        String title = null;

        doThrow(Exception.class)
                .when(filmDAO).deleteFilm(title);
        doThrow(Exception.class)
                .when(directorDAO).deleteDirector(title);
        doThrow(Exception.class)
                .when(filmDAO).existingTitle().isEmpty();
    }

    @Test
    public void updateExistingFilmTest() {
        doNothing().when(filmDAO).updateFilm(anyString(), anyString());
        doNothing().when(directorDAO).updateFilm(anyString(), anyString());
    }

    @Test(expected = Exception.class)
    public void updateExistingFilmNegativeTest() throws NoSuchMethodException {
        String oldTitle = "";
        String newTitle = null;

        doThrow(Exception.class)
                .when(filmDAO).updateFilm(oldTitle, newTitle);
        doThrow(Exception.class)
                .when(directorDAO).updateFilm(oldTitle, newTitle);
        doThrow(Exception.class)
                .when(filmDAO).existingTitle().isEmpty();
    }

    @Test
    public void showAllTest() throws NoSuchMethodException {
        filmDAO.getAllFilms();
        Mockito.verify(filmDAO).getAllFilms();
    }

    @Test(expected = Exception.class)
    public void showAllNegativeTest() throws NoSuchMethodException {
        doThrow(Exception.class)
                .when(filmDAO).getAllFilms().isEmpty();
    }

    @Test
    public void showByTitleTest() throws InterruptedException {
        String title = "Test";
        filmDAO.getFilm(title);
        Mockito.verify(filmDAO).getFilm(title);
    }

    @Test(expected = Exception.class)
    public void showByTitleNegativeTest() throws NoSuchMethodException, InterruptedException {
        String title = null;

        doThrow(Exception.class)
                .when(filmDAO).getFilm(title);
        doThrow(Exception.class)
                .when(filmDAO).existingTitle().isEmpty();
    }
}
