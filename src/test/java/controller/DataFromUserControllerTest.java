package controller;

import films.dao.DirectorDAO;
import films.dao.FilmDAO;
import films.ds.DirectorDs;
import films.ds.FilmDs;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DataFromUserControllerTest {

    @Mock
    FilmDs film;
    @Mock
    FilmDAO filmDAO;
    @Mock
    DirectorDs director;
    @Mock
    DirectorDAO directorDAO;

    @Before
    public void testInitialization() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void collectDataTest() throws Exception{
        doAnswer((i) -> {
            assertTrue("tytul".equals(i.getArgument(0)));
            return null;
        }).when(film).setTytul(anyString());
        when(film.getTytul()).thenReturn("tytul");
        film.setTytul("tytul");
        assertEquals("tytul", film.getTytul());

        when(film.getRozmiar()).thenReturn(258.95);
        film.setRozmiar(258.95);
        assertEquals(258.95, film.getRozmiar(), 0.0);

        when(film.getCzasTrwania()).thenReturn("01:02:30");
        film.setCzasTrwania("01:02:30");
        assertEquals("01:02:30", film.getCzasTrwania());

        doAnswer((i) -> {
            assertTrue("test testowy".equals(i.getArgument(0)));
            return null;
        }).when(director).setImieInazwisko(anyString());
        when(director.getImieInazwisko()).thenReturn("test testowy");
        film.setCzasTrwania("test testowy");
        assertEquals("test testowy", director.getImieInazwisko());
        doNothing().when(filmDAO).insert(film);
        doNothing().when(directorDAO).insert(director);
    }

    @Test(expected = Exception.class)
    public void collectDataNegativeTest() {
        doThrow(NullPointerException.class)
                .when(film).setTytul(null);
        doThrow(NumberFormatException.class)
                .when(film).setRozmiar(anyFloat());
        doThrow(Exception.class)
                .when(film).setCzasTrwania("01");
        doThrow(NullPointerException.class)
                .when(director).setImieInazwisko(null);
    }
}
