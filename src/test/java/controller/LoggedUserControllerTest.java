package controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;

import java.security.Principal;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WithMockUser(username = "admin", roles = "ADMIN")
public class LoggedUserControllerTest {

    @Mock
    Authentication authentication;
    @Mock
    SecurityContext securityContext;
    @Mock
    Principal principal;

    @Before
    public void testInitialization() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void loggedUsernameAndVerifyTest() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(SecurityContextHolder.getContext().getAuthentication().getName()).thenReturn("admin");
        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(principal);
    }

    @Test(expected = Exception.class)
    public void loggedUsernameAndVerifyNegativeTest() {
        doReturn(authentication).when(securityContext).getAuthentication().getName().equals("");
        doReturn(authentication).when(securityContext).getAuthentication().getPrincipal().equals(null);
    }
}
