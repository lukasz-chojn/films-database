package controller;

import films.dao.UsersDAO;
import films.ds.UsersDs;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UsersControllerTest {

    @Mock
    UsersDAO usersDAO;
    @Mock
    UsersDs usersDs;

    @Before
    public void testInitialization() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addUserTest() {
        doAnswer((i) -> {
            assertTrue("user".equals(i.getArgument(0)));
            return null;
        }).when(usersDs).setUsername(anyString());
        when(usersDs.getUsername()).thenReturn("user");
        usersDs.setUsername("user");
        assertEquals("user", usersDs.getUsername());

        doAnswer((i) -> {
            assertTrue("pass".equals(i.getArgument(0)));
            return null;
        }).when(usersDs).setPassword(anyString());
        when(usersDs.getPassword()).thenReturn("pass");
        usersDs.setPassword("pass");
        assertEquals("pass", usersDs.getPassword());

        doAnswer((i) -> {
            assertTrue("user".equals(i.getArgument(0)));
            return null;
        }).when(usersDs).setRole(anyString());
        when(usersDs.getRole()).thenReturn("user");
        usersDs.setRole("user");
        assertEquals("user", usersDs.getRole());
    }

    @Test(expected = Exception.class)
    public void addUserNegativeTest() {

        doThrow(Exception.class)
                .when(usersDs).setUsername(null);
        doThrow(Exception.class)
                .when(usersDs).setPassword("");
        doThrow(Exception.class)
                .when(usersDs).setRole("");
    }

    @Test
    public void getAllUsersTest() {
        usersDAO.findAll();
        Mockito.verify(usersDAO).findAll();
    }

    @Test
    public void getUserByUserNameTest() {
        String user = "user";
        usersDAO.findByUsernameContaining(user);
        verify(usersDAO).findByUsernameContaining(user);
    }

    @Test(expected = Exception.class)
    public void getUserByUserNameNegativeTest() {
        doThrow(Exception.class)
                .when(usersDAO).findByUsernameContaining(any());
    }

    @Test
    public void updatePasswordTest() {
        doNothing().when(usersDAO).updatePassword(anyString(), anyString());
    }

    @Test(expected = Exception.class)
    public void updatePasswordNegativeTest() {
        String user = "";
        String pass = "";

        doThrow(Exception.class)
                .when(usersDAO).updatePassword(pass, user);
    }

    @Test
    public void updateUserRoleTest() {
        doNothing().when(usersDAO).updateRole(anyString(), anyString());
    }

    @Test(expected = Exception.class)
    public void updateUserRoleNegativeTest() {
        String user = "";
        String role = "";

        doThrow(Exception.class)
                .when(usersDAO).updateRole(role, user);
    }

    @Test
    public void deleteByUsernameTest() {
        doNothing().when(usersDAO).deleteByUsername(anyString());
    }

    @Test(expected = Exception.class)
    public void deleteByUsernameNegativeTest() {
        doThrow(Exception.class)
                .when(usersDAO).deleteByUsername(null);
    }
}
