package validators;

import films.helper.UserRole;
import films.validators.UsersValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UsersValidatorTest {

    private final String USER_REGEXP = "^[A-Za-z0-9_]{0,50}$";
    private final String PASS_REGEXP = "^[A-Za-z0-9_]{3,10}$";

    private boolean user;
    private boolean pass;

    @Mock
    UsersValidator usersValidator;

    @Before
    public void testInitialization() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void validatorUsersValidatorPositiveTest() {
        when(usersValidator.getRole()).thenReturn("user");
        usersValidator.setRole("user");
        assertEquals("user", usersValidator.getRole());

        List<String> roleList = new ArrayList<>();
        for (UserRole role : UserRole.values()) {
            String s = role.getRole();
            roleList.add(s);
        }
        assertNotNull(roleList);

        user = Pattern.matches(USER_REGEXP, "test");
        assertTrue(user);

        pass = Pattern.matches(PASS_REGEXP, "password");
        assertTrue(pass);
    }

    @Test(expected = Exception.class)
    public void validatorUsersValidatorNegativeTest() {
        doReturn(Exception.class).when(usersValidator).setRole(null);
        doReturn(Exception.class).when(usersValidator).setUser("");
        doReturn(Exception.class).when(usersValidator).setPassword(null);
        doReturn(Exception.class).when(usersValidator).setConfirmPassword("");

        user = Pattern.matches(USER_REGEXP, "jan k");
        assertFalse(user);

        pass = Pattern.matches(PASS_REGEXP, "password123");
        assertFalse(pass);
    }
}
