package validators;

import films.validators.DataFromUserValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.regex.Pattern;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DataFromUsersValidatorTest {

    private final String CZAS_REGEXP = "(?:[01]\\d|2[0123]):(?:[012345]\\d):(?:[012345]\\d)";
    private final String REZYSER_REGEXP = "^\\D+$";
    private final String ROZMIAR_REGEXP = "^\\d{1,6}+((\\.|,)+\\d{0,2}+)?$";

    @Mock
    DataFromUserValidator dataFromUserValidator;


    @Before
    public void testInitialization() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void validatorDataFromUserPositiveTest() {
        boolean czasTest;
        boolean rezyserTest;
        boolean rozmiarDotTest;
        boolean rozmiarComaTest;

        doNothing().when(dataFromUserValidator).setTytul(anyString());
        when(dataFromUserValidator.getTytul()).thenReturn("tytul");
        dataFromUserValidator.setTytul("tytul");
        assertEquals("tytul", dataFromUserValidator.getTytul());

        czasTest = Pattern.matches(CZAS_REGEXP, "01:20:25");
        assertTrue(czasTest);

        rezyserTest = Pattern.matches(REZYSER_REGEXP, "Jan Testowy");
        assertTrue(rezyserTest);

        rozmiarDotTest = Pattern.matches(ROZMIAR_REGEXP, "256.96");
        assertTrue(rozmiarDotTest);

        rozmiarComaTest = Pattern.matches(ROZMIAR_REGEXP, "256,96");
        assertTrue(rozmiarComaTest);
    }

    @Test(expected = Exception.class)
    public void validatorDataFromUserNegativeTest() {
        boolean czas;
        boolean rozmiar;
        boolean rezyser;

        doThrow(Exception.class).when(dataFromUserValidator).setTytul("");
        doThrow(Exception.class).when(dataFromUserValidator).setCzas(null);
        doThrow(Exception.class).when(dataFromUserValidator).setRozmiar("");
        doThrow(Exception.class).when(dataFromUserValidator).setRezyser(null);

        czas = Pattern.matches(CZAS_REGEXP, "01:21;42");
        assertFalse(czas);

        rozmiar = Pattern.matches(ROZMIAR_REGEXP, "1234567-21");
        assertFalse(rozmiar);

        rezyser = Pattern.matches(REZYSER_REGEXP, "205 47");
        assertFalse(rezyser);
    }
}
