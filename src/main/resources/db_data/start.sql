INSERT INTO users (ID, PASSWORD, ROLE, USERNAME) VALUES
(1, '$2a$10$tmSX4HNnIbvkZWBoW0zvH.i2OGnqWnOyq31fr4Ym33c5WckaXrRBG','admin','admin'),
(2, '$2a$10$LoD776eIimmJG5.jzVDanurnC1HC0Ba0y4a3kNcO63smdwSmjOiuq','user','user');
ALTER TABLE director DROP FOREIGN KEY FK_tytul;
INSERT INTO director (id, imieInazwisko, tytul) VALUES (1, 'Jan Testowy', 'test');
INSERT INTO film (id, tytul, czasTrwania, rozmiar) VALUES (1, 'test', '01:25:42', 254.52);
ALTER TABLE director ADD CONSTRAINT FK_tytul FOREIGN KEY (tytul) REFERENCES film(tytul);