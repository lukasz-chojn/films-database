package films.exceptions;

import films.helper.HeaderHelper;
import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.SQLException;

/**
 * klasa do obsługi błędów w aplikacji
 */
@RestControllerAdvice
public class ExceptionHandlerController extends Throwable {

    private Logger logger = Logger.getLogger(ExceptionHandlerController.class);

    @Autowired
    private HeaderHelper headerHelper;

    public ExceptionHandlerController() {
        logger.info("Startująca klasa " + ExceptionHandlerController.class.getSimpleName());
    }

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<String> bladWalidacjiRozmiaru() {
        logger.error("Zły format liczby. Komunikat zbudowany w klasie  " + ExceptionHandlerController.class.getSimpleName());
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .headers(headerHelper.headers())
                .body("Zły format liczby");
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> bladZapytaniaDoBazy() {
        logger.error("Błąd zapytania do bazy danych. Komunikat zbudowany w klasie  " + ExceptionHandlerController.class.getSimpleName());
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .headers(headerHelper.headers())
                .body("Błąd zapytania do bazy danych. Naruszenie integralności danych");
    }

    @ExceptionHandler(SQLException.class)
    public ResponseEntity<String> bladZapytaniaSQL() {
        logger.error("Wystąpił błąd zapytania SQL-owego. Komunikat zbudowany w klasie " + ExceptionHandlerController.class.getSimpleName());
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .headers(headerHelper.headers())
                .body("Błąd zapytania SQL");
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<String> bladNulla() {
        logger.error("Wystąpił null pointer. Komunikat zbudowany w klasie  " + ExceptionHandlerController.class.getSimpleName());
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .headers(headerHelper.headers())
                .body("Wystąpił null pointer");
    }

    @ExceptionHandler(IllegalStateException.class)
    public ResponseEntity<String> bladIllegalStateException() {
        logger.error("Błędnie wypełniony formularz. Komunikat zbudowany w klasie  " + ExceptionHandlerController.class.getSimpleName());
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .headers(headerHelper.headers())
                .body("Błędnie wypełniony formularz. Sprawdź wszystkie pola czy są poprawnie wypełnione " +
                        "lub czy użytkownik znajduje się w bazie");

    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> bladWejsciaWyjscia() {
        logger.error("Nastąpiła nieudana próba zapisu i/lub odczytu danych. Komunikat zbudowany w klasie  " + ExceptionHandlerController.class.getSimpleName());
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .headers(headerHelper.headers())
                .body("Nastąpiła nieudana próba zapisu i/lub odczytu danych");
    }

    @ExceptionHandler(ServletException.class)
    public ResponseEntity<String> bladServletu() {
        logger.error("Wystąpił problem z servletem. Komunikat zbudowany w klasie " + ExceptionHandlerController.class.getSimpleName());
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .headers(headerHelper.headers())
                .body("Wystąpił problem z servletem");
    }
}
