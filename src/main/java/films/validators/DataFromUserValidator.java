package films.validators;

import org.apache.log4j.Logger;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * klasa walidująca dane przesłane od użytkownika dodającego film
 */
public class DataFromUserValidator {

    private Logger logger = Logger.getLogger(DataFromUserValidator.class);

    private String tytul;
    private String czas;
    private String rezyser;
    private String rozmiar;
    private Double filmSize;

    @NotEmpty(message = "Tytuł nie może być pusty")
    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    @NotEmpty(message = "Czas nie może być pusty")
    @Pattern(regexp = "(?:[01]\\d|2[0123]):(?:[012345]\\d):(?:[012345]\\d)", message = "Czas musi być w formacie HH:MM:SS")
    public String getCzas() {
        return czas;
    }

    public void setCzas(String czas) {
        this.czas = czas;
    }

    @NotEmpty(message = "Reżyser nie może być pusty")
    @Pattern(regexp = "^\\D+$", message = "Imię i nazwisko może składać się tylko z liter")
    public String getRezyser() {
        return rezyser;
    }

    public void setRezyser(String rezyser) {
        this.rezyser = rezyser;
    }

    @NotEmpty(message = "Rozmiar nie może być pusty")
    @Pattern(regexp = "^(?!0)\\d{1,6}+((\\.|,)+\\d{0,2}+)?$", message = "Rozmiar musi być liczbą większą od zera")
    private String getRozmiar() {
        return rozmiar;
    }

    public void setRozmiar(String rozmiar) {
        this.rozmiar = rozmiar;
    }

    public Double getFilmSize() {
        return filmSize;
    }

    public void setFilmSize(Double filmSize) {
        this.filmSize = filmSize;
    }

    public Double parseSize() {
        if (getRozmiar().contains(",")) {
            String size = getRozmiar().replace(",", ".");
            filmSize = Double.parseDouble(size);
        } else {
            try {
                filmSize = Double.parseDouble(getRozmiar());
            } catch (NumberFormatException nfe) {
                logger.error("Błędny format liczby zweryfikowany w " + DataFromUserValidator.class.getSimpleName());
            }
        }
        return filmSize;
    }
}
