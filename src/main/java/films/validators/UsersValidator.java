package films.validators;

import films.helper.UserRole;
import org.apache.log4j.Logger;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * klasa walidująca dane przesłane do dodania nowego użytkownika
 */
public class UsersValidator {
    private Logger logger = Logger.getLogger(UsersValidator.class);

    private String user;
    private String password;
    private String confirmPassword;
    private String role;

    @NotEmpty(message = "Pole z nazwą użytkownika nie może być puste")
    @Pattern(regexp = "^[A-Za-z0-9_]{0,50}$", message = "Nazwa użytkownika może mieć maksymalnie 50 znaków i zawierać wyłącznie cyfry, litery i znak \"_\"")
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @NotEmpty(message = "Pole z hasłem nie może być puste")
    @Pattern(regexp = "^[A-Za-z0-9_]{3,10}$", message = "Hasło może mieć minimalnie 3 i maksymalnie 10 znaków oraz zawierać wyłącznie cyfry, litery i znak \"_\"")
    public String getPassword() {
        return password;
    }

    public String setPassword(String password) {
        this.password = password;
        return password;
    }

    @NotEmpty(message = "Pole \"Powtórz hasło\" nie może być puste")
    @Pattern(regexp = "^[A-Za-z0-9_]{3,10}$", message = "Hasło może mieć minimalnie 3 i maksymalnie 10 znaków oraz zawierać wyłącznie cyfry, litery i znak \"_\"")
    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @NotEmpty(message = "Pole z rolą nie może być puste")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String verifyRoleFromForm() {
        if (UserRole.ADMIN.getRole().equals(getRole())) {
            setRole("admin");
        } else if (UserRole.USER.getRole().equals(getRole())) {
            setRole("user");
        } else {
            logger.error("Nie obsługiwana rola w systemie zweryfikowana w " + UsersValidator.class.getSimpleName());
        }
        return getRole();
    }

    public String checkPassword() {
        if (getPassword().equals(getConfirmPassword())) {
            setPassword(getPassword());
        } else {
            logger.error("Hasła nie pasują do siebie zweryfikowane w " + UsersValidator.class.getSimpleName());
        }
        return setPassword(getPassword());
    }
}