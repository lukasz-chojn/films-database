package films.configuration;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.accept.ContentNegotiationManagerFactoryBean;
import org.springframework.web.accept.ContentNegotiationStrategy;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.DefaultServletHttpRequestHandler;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * klasa konfiguracyjna. Odpowiada za dostęp do Hibernate'a i konfigurację Springa
 */
@Configuration
@ComponentScan(basePackages = {"films"})
@PropertySource(value = {"classpath:dbconfig.properties", "classpath:application.properties"})
@EnableTransactionManagement
@EnableWebMvc
@EnableJpaRepositories(basePackages = "films", entityManagerFactoryRef = "entityManagerBean")
public class Config implements WebMvcConfigurer {

    private Logger logger = Logger.getLogger(Config.class);

    @Autowired
    private Environment environment;

    @Bean
    public DataSource source() {
        logger.info("Podnoszę bean'a " + DataSource.class.getSimpleName());
        DriverManagerDataSource source = new DriverManagerDataSource();
        logger.info("Ładuję sterownik bazodanowy z użyciem " + DriverManagerDataSource.class.getSimpleName());
        source.setDriverClassName(environment.getProperty("driver"));
        source.setUrl(environment.getProperty("url"));
        source.setUsername(environment.getProperty("user"));
        source.setPassword(environment.getProperty("pass"));
        return source;
    }

    @Bean
    public DataSourceInitializer dataSourceInitializer() {
        logger.info("Podnoszę bean'a " + DataSourceInitializer.class.getSimpleName());

        DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator();

        logger.info("Dodaję dane startowe do aplikacji z użyciem " + ResourceDatabasePopulator.class.getSimpleName());

        resourceDatabasePopulator.addScript(new ClassPathResource("/db_data/start.sql"));

        dataSourceInitializer.setDataSource(source());
        dataSourceInitializer.setDatabasePopulator(resourceDatabasePopulator);
        return dataSourceInitializer;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerBean() {
        logger.info("Podnoszę bean'a " + LocalContainerEntityManagerFactoryBean.class.getSimpleName());

        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        Properties properties = new Properties();

        logger.info("Rozpoczynam konfigurację środowiska dla Hibernate z użyciem " + LocalContainerEntityManagerFactoryBean.class.getSimpleName());
        entityManagerFactoryBean.setPersistenceUnitName("filmsDatabase");
        entityManagerFactoryBean.setDataSource(source());
        entityManagerFactoryBean.setPackagesToScan("films");

        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setGenerateDdl(true);
        adapter.setShowSql(true);

        entityManagerFactoryBean.setJpaVendorAdapter(adapter);
        entityManagerFactoryBean.setJpaProperties(properties);

        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.connection.useUnicode", "true");
        properties.put("hibernate.connection.characterEncoding", "UTF-8");
        properties.put("hibernate.connection.charSet", "UTF-8");
        properties.put("hibernate.search.default.directory_provider", "filesystem");
        properties.put("hibernate.search.default.indexBase", "/indexes");
        properties.put("hibernate.hbm2ddl.auto", "create");
        properties.put("spring.h2.console.enabled", true);
        properties.put("spring.h2.console.path", "/h2");
        properties.put("spring.jpa.show-sql", "true");

        logger.info("Kończę konfigurację środowiska dla Hibernate z użyciem " + LocalContainerEntityManagerFactoryBean.class.getSimpleName());
        return entityManagerFactoryBean;
    }

    @Bean
    public JpaTransactionManager transactionManager() {
        logger.info("Podnoszę bean'a " + JpaTransactionManager.class.getSimpleName());
        JpaTransactionManager manager = new JpaTransactionManager();
        manager.setEntityManagerFactory(entityManagerBean().getObject());
        return manager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        logger.info("Podnoszę bean'a " + PersistenceExceptionTranslationPostProcessor.class.getSimpleName());
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    public ViewResolver configureViewResolver() {
        logger.info("Podnoszę bean'a " + ViewResolver.class.getSimpleName());
        InternalResourceViewResolver viewResolve = new InternalResourceViewResolver();
        viewResolve.setPrefix("/WEB-INF/");
        viewResolve.setSuffix(".html");
        return viewResolve;
    }

    @Bean
    public DefaultServletHttpRequestHandler defaultServletHandlerConfigurer() {
        logger.info("Podnoszę bean'a " + DefaultServletHttpRequestHandler.class.getSimpleName());
        return new DefaultServletHttpRequestHandler();
    }

    @Bean
    public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
        logger.info("Podnoszę bean'a " + ByteArrayHttpMessageConverter.class.getSimpleName());
        return new ByteArrayHttpMessageConverter();
    }

    @Bean
    public ResourceHttpMessageConverter resourceHttpMessageConverter() {
        logger.info("Podnoszę bean'a " + ResourceHttpMessageConverter.class.getSimpleName());
        return new ResourceHttpMessageConverter();
    }

    @Bean
    public SourceHttpMessageConverter<javax.xml.transform.Source> sourceHttpMessageConverter() {
        logger.info("Podnoszę bean'a " + SourceHttpMessageConverter.class.getSimpleName());
        return new SourceHttpMessageConverter<>();
    }

    @Bean
    public FormHttpMessageConverter formHttpMessageConverter() {
        logger.info("Podnoszę bean'a " + FormHttpMessageConverter.class.getSimpleName());
        return new FormHttpMessageConverter();
    }

    @Bean
    public StringHttpMessageConverter stringHttpMessageConverter() {
        logger.info("Podnoszę bean'a " + StringHttpMessageConverter.class.getSimpleName());
        return new StringHttpMessageConverter();
    }

    @Bean
    public ContentNegotiationManagerFactoryBean contentNegotiationConfigurer() {
        logger.info("Podnoszę bean'a " + ContentNegotiationManagerFactoryBean.class.getSimpleName());
        ContentNegotiationManagerFactoryBean contentNegotiationManagerFactoryBean = new ContentNegotiationManagerFactoryBean();
        contentNegotiationManagerFactoryBean.setFavorParameter(true);
        contentNegotiationManagerFactoryBean.setFavorPathExtension(false);
        contentNegotiationManagerFactoryBean.setIgnoreAcceptHeader(false);
        return contentNegotiationManagerFactoryBean;
    }

    @Bean
    public ContentNegotiationStrategy contentNegotiationStrategy() {
        logger.info("Podnoszę bean'a " + ContentNegotiationStrategy.class.getSimpleName());
        return webRequest -> {
            List<MediaType> mediaTypeList = new ArrayList<>();
            mediaTypeList.add(MediaType.APPLICATION_JSON_UTF8);
            mediaTypeList.add(MediaType.APPLICATION_FORM_URLENCODED);
            mediaTypeList.add(MediaType.TEXT_HTML);
            return mediaTypeList;
        };
    }

    @Bean
    public ServletRegistrationBean<org.h2.server.web.WebServlet> h2servletRegistration() {
        logger.info("Podnoszę bean'a " + ServletRegistrationBean.class.getSimpleName());
        ServletRegistrationBean<org.h2.server.web.WebServlet> registration = new ServletRegistrationBean<>(new org.h2.server.web.WebServlet());
        logger.info("Inicjalizacja servletu dla bazy danych H2 " + org.h2.server.web.WebServlet.class.getSimpleName());
        registration.addUrlMappings("/h2/*");
        registration.addInitParameter("webAllowOthers", "true");
        registration.addInitParameter("webPort", "8000");
        return registration;
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        logger.info("Startująca klasa " + ResourceHandlerRegistry.class.getSimpleName());
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
        registry.addResourceHandler("/**").addResourceLocations("/WEB-INF/", "/WEB-INF/film_and_director/", "/WEB-INF/users/");
        registry.addResourceHandler("/css/**").addResourceLocations("/WEB-INF/css/", "/WEB-INF/error_pages/css/");
        registry.addResourceHandler("/js/**").addResourceLocations("/WEB-INF/js/", "/WEB-INF/error_pages/js/");
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}