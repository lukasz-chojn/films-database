package films.configuration;

import org.apache.log4j.Logger;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

/**
 * klasa tworząca instancję przekierowującą na wskazany URL po udanym zalogowaniu
 */
@Component
public class AuthenticationHandler {

    private Logger logger = Logger.getLogger(AuthenticationHandler.class);

    public AuthenticationSuccessHandler authenticationSuccessHandler() {
        logger.info("Startująca klasa " + AuthenticationHandler.class.getSimpleName());
        SimpleUrlAuthenticationSuccessHandler simpleUrlAuthenticationSuccessHandler = new SimpleUrlAuthenticationSuccessHandler();
        simpleUrlAuthenticationSuccessHandler.setDefaultTargetUrl("/loggedUser.html");
        return simpleUrlAuthenticationSuccessHandler;
    }
}
