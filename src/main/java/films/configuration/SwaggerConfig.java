package films.configuration;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Klasa konfiguracyjna do obsługi Swaggera
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private Logger logger = Logger.getLogger(SwaggerConfig.class);

    @Bean
    public Docket api() {
        logger.info("Uruchomiono Swaggera z poziomu klasy " + SwaggerConfig.class.getSimpleName());
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("films.controller"))
                .paths(PathSelectors.regex("/.*"))
                .build().apiInfo(about());

    }

    private ApiInfo about() {
        return new ApiInfoBuilder()
                .title("Films database app")
                .description("A films database created for educational purpose. This application can use H2, MySQL or PostgreSQL database and java 8 language." +
                        "Everything is worked with SpringBoot and Spring MVC Framework.")
                .version("5.0")
                .build();
    }
}
