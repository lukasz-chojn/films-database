package films.configuration;

import films.helper.LogoutHelper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;

import javax.sql.DataSource;

/**
 * klasa odpowiada za konfigurację zabezpieczeń w aplikacji
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private Logger logger = Logger.getLogger(SecurityConfig.class);

    @Autowired
    private DataSource dataSource;
    @Autowired
    private LogoutHelper logoutHelper;
    @Autowired
    private CorsFilter corsFilter;
    @Autowired
    private AuthenticationHandler authenticationHandler;

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        logger.info("Startująca klasa " + AuthenticationManagerBuilder.class.getSimpleName());
        authenticationManagerBuilder
                .jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("SELECT username, password, true FROM users WHERE username=?")
                .authoritiesByUsernameQuery("SELECT username, role FROM users WHERE username=?")
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        logger.info("Inicjalizacja enkodera haseł " + BCryptPasswordEncoder.class.getSimpleName());
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        logger.info("Konfiguracja zabezpieczeń dla " + HttpSecurity.class.getSimpleName());
        httpSecurity
                .addFilterBefore(corsFilter, ChannelProcessingFilter.class)
                .authorizeRequests()
                .antMatchers("/{\\w+}/add{^[A-Za-z]+$}.html")
                .access("hasAuthority('user') or hasAuthority('admin')")
                .antMatchers("/{\\w+}/update{^[A-Za-z]+$}.html")
                .access("hasAuthority('admin')")
                .antMatchers("/{\\w+}/delete{^[A-Za-z]+$}.html")
                .access("hasAuthority('admin')")
                .antMatchers("/admin.html")
                .access("hasAuthority('admin')")
                .antMatchers("/user.html")
                .access("hasAuthority('user') or hasAuthority('admin')")
                .and()
                .formLogin().loginPage("/login.html").loginProcessingUrl("/login")
                .successHandler(authenticationHandler.authenticationSuccessHandler()).failureUrl("/error_pages/failLogin.html")
                .usernameParameter("username").passwordParameter("password")
                .and()
                .logout()
                .logoutSuccessHandler(logoutHelper).clearAuthentication(true).deleteCookies("JSESSIONID")
                .and()
                .exceptionHandling().accessDeniedPage("/error_pages/403.html")
                .and()
                .csrf().disable();
    }
}
