package films.controller;

import com.google.gson.Gson;
import films.ds.DirectorDs;
import films.ds.FilmDs;
import films.helper.HeaderHelper;
import films.helper.MessageBuilderHelper;
import films.service.TransactionService;
import films.validators.DataFromUserValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Klasa zbierająca dane od użytkownika
 */
@RestController
@RequestMapping("/film")
public class DataFromUserController {

    private Logger logger = Logger.getLogger(DataFromUserController.class);

    @Autowired
    private HeaderHelper helper;
    @Autowired
    private TransactionService transactionService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<String> collectData(@Valid DataFromUserValidator dataFromUserValidator, BindingResult bindingResult) throws Exception {

        FilmDs film = new FilmDs();
        DirectorDs director = new DirectorDs();

        logger.info("Inicjalizacja obiektów " + FilmDs.class.getSimpleName() + " oraz " + DirectorDs.class.getSimpleName() + " w klasie " + DataFromUserController.class.getSimpleName());

        List<String> bledy = new ArrayList<>();

        if (bindingResult.hasFieldErrors()) {
            for (ObjectError error : bindingResult.getAllErrors()) {
                bledy.add(error.getDefaultMessage());
            }
        } else {
            film.setTytul(dataFromUserValidator.getTytul().toLowerCase());
            director.setFilm(film);
            director.setImieInazwisko(dataFromUserValidator.getRezyser());
            film.setCzasTrwania(dataFromUserValidator.getCzas());
            film.setRozmiar(dataFromUserValidator.parseSize());

            logger.info("Wykonuję zapis do bazy w klasie " + this.getClass().getSimpleName());

            transactionService.insertService(film, director);
            return ResponseEntity.status(HttpStatus.CREATED).headers(helper.headers()).body(new Gson().toJson("Film został dodany do tabeli filmów i reżyserów"));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(new MessageBuilderHelper(bledy).toString());
    }
}
