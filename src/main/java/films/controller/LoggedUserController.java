package films.controller;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Klasa weryfikująca użytkowników
 */
@RestController
@RequestMapping("/logged")
public class LoggedUserController {

    public static final String ANONYMOUS_USER = "anonymousUser";
    public static final String NOT_ROLE_FOR_USER = "Brak zdefiniowanej roli";
    private Logger logger = Logger.getLogger(LoggedUserController.class);

    /**
     * Metoda pobiera login zalogowanego użytkownika
     *
     * @return login zalogowanego użytkownika
     */
    @RequestMapping(value = "/username", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public String loggedUsername() {
        return getLoggedUsername();
    }


    /**
     * Metoda weryfikuje login i rolę w systemie
     * @return rola w systemie
     */
    @RequestMapping(value = "/verify", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public String verify() {
        if (isLoggedin()) {
            return userRole();
        }
        return NOT_ROLE_FOR_USER;
    }

    private String userRole() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
    }

    private boolean isLoggedin() {
        return !(getLoggedUsername().equals(ANONYMOUS_USER));
    }

    private String getLoggedUsername() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
