package films.controller;

import com.google.common.collect.Iterables;
import com.google.gson.Gson;
import films.dao.UsersDAO;
import films.ds.UsersDs;
import films.exceptions.ExceptionHandlerController;
import films.helper.HeaderHelper;
import films.helper.MessageBuilderHelper;
import films.helper.SearchAdminHelper;
import films.validators.UsersValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * kontroler służący do wszelkich operacji na użytkownikach
 */
@RestController
@RequestMapping(value = "/users")
public class UsersController {

    private Logger logger = Logger.getLogger(UsersController.class);
    private final Gson GSON = new Gson();

    @Autowired
    private UsersDAO usersDAO;
    @Autowired
    private HeaderHelper headerHelper;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private SearchAdminHelper searchAdminHelper;
    @Autowired
    private MessageBuilderHelper messageBuilderHelper;


    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<String> addUser(@Valid UsersValidator usersValidator, BindingResult bindingResult) {
        UsersDs users = new UsersDs();
        logger.info("Inicjalizacja obiektu " + users + " w klasie " + UsersController.class.getSimpleName());

        List<String> bledy = new ArrayList<>();

        if (bindingResult.hasFieldErrors()) {
            for (ObjectError error : bindingResult.getAllErrors()) {
                bledy.add(error.getDefaultMessage());
            }
        } else {
            users.setUsername(usersValidator.getUser());
            users.setPassword(passwordEncoder.encode(usersValidator.checkPassword()));
            users.setRole(usersValidator.verifyRoleFromForm());
            logger.info("Wykonuję zapis do bazy w klasie " + this.getClass().getSimpleName());
            usersDAO.save(users);
            return ResponseEntity.status(HttpStatus.CREATED).headers(headerHelper.headers()).body(GSON.toJson("Użytkownik został dodany do bazy."));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(headerHelper.headers()).body(new MessageBuilderHelper(bledy).toString());
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Iterable<UsersDs> getAllUsers() throws ExceptionHandlerController {
        Iterable<UsersDs> usersDsList = usersDAO.findAll();
        if (Iterables.isEmpty(usersDsList)) {
            throw new ExceptionHandlerController();
        }
        return usersDsList;
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Iterable<UsersDs> getUserByUserName(@RequestParam("user") String user) throws ExceptionHandlerController {
        if (usersDAO.findByUsernameContaining(user).isEmpty() || user.isEmpty()) {
            throw new ExceptionHandlerController();
        }
        return usersDAO.findByUsernameContaining(user);
    }

    @RequestMapping(value = "/updatepass", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<String> updatePassword(@RequestParam("newpass") String newPassword,
                                                 @RequestParam("user") String user) throws ExceptionHandlerController {
        List<String> usersList = usersDAO.getAllUsernames();
        if (user.isEmpty() || !(usersList.contains(user))) {
            throw new ExceptionHandlerController();
        } else if (newPassword.isEmpty()) {
            throw new ExceptionHandlerController();
        } else {
            String encodedPass = passwordEncoder.encode(newPassword);
            logger.info("Wykonuję aktualizację wpisu z hasłem w bazie w klasie " + UsersController.class.getSimpleName());
            usersDAO.updatePassword(encodedPass, user);
            return ResponseEntity.status(HttpStatus.OK).headers(headerHelper.headers()).body(GSON.toJson("Hasło zostało zaktualizowane"));
        }
    }

    @RequestMapping(value = "/updaterole", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<String> updateUserRole(@RequestParam("newrole") String newRole,
                                                 @RequestParam("user") String user) throws ExceptionHandlerController {
        List<String> usersList = usersDAO.getAllUsernames();
        if (user.isEmpty() || !(usersList.contains(user))) {
            throw new ExceptionHandlerController();
        } else if (newRole.isEmpty()) {
            throw new ExceptionHandlerController();
        } else {
            logger.info("Wykonuję aktualizację wpisu z rolą w bazie w klasie " + UsersController.class.getSimpleName());
            usersDAO.updateRole(newRole, user);
            return ResponseEntity.status(HttpStatus.OK).headers(headerHelper.headers()).body(GSON.toJson("Rola została zaktualizowana"));
        }
    }

    @RequestMapping(value = "/delete/{user}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteUserFromDatabase(@PathVariable("user") String user) throws ExceptionHandlerController {
        List<String> usersList = usersDAO.getAllUsernames();
        List<String> admins = searchAdminHelper.checkExistingAdmins();
        if (user.isEmpty() || !(usersList.contains(user))) {
            throw new ExceptionHandlerController();
        } else if (admins.size() == 1) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(headerHelper.headers()).body("W systemie jest " +
                    "tylko jeden administrator. Nie możesz go usunąć");
        } else {
            logger.info("Wykonuję usunięcie wpisu z bazy w klasie " + UsersController.class.getSimpleName());
            usersDAO.deleteByUsername(user);
            return ResponseEntity.status(HttpStatus.OK).headers(headerHelper.headers()).body(GSON.toJson("Użytkownik usunięty"));
        }
    }
}