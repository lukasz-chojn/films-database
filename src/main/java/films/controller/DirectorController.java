package films.controller;

import com.google.gson.Gson;
import films.dao.DirectorDAO;
import films.dao.FilmDAO;
import films.helper.HeaderHelper;
import films.service.TransactionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Klasa aktualizująca dane reżysera filmu w tabeli reżyserów
 */
@RestController
@RequestMapping("/director")
public class DirectorController {

    private Logger logger = Logger.getLogger(DirectorController.class);

    @Autowired
    private DirectorDAO directorDao;
    @Autowired
    private FilmDAO filmDao;
    @Autowired
    private HeaderHelper helper;
    @Autowired
    private TransactionService transactionService;

    /**
     * metoda wykonjąca update na tabeli z reżyserami
     * @param title - tytuł, dla którego chcemy zmienić reżysera
     * @param director - dane reżysera
     * @return ok jeżeli operacja udana
     */
    @RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<String> updateDirector(
            @RequestParam("title") String title,
            @RequestParam("director") String director) throws NoSuchMethodException {

        List<String> listTitle = filmDao.existingTitle();
        if (!(listTitle.contains(title))) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).headers(helper.headers()).body("Brak filmu w bazie");
        } else if (title.isEmpty() || director.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body("Nie wypełniono wszystkich wymaganych pól");
        } else {
            logger.info("Wykonuję aktualizację wpisu w bazie w klasie " + DirectorController.class.getSimpleName());
            transactionService.directorUpdateService(title, director);
        }
        return ResponseEntity.status(HttpStatus.OK).headers(helper.headers()).body(new Gson().toJson("Reżyser zaktualizowany"));
    }
}