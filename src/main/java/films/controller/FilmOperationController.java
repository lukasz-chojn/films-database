package films.controller;

import com.google.gson.Gson;
import films.dao.DirectorDAO;
import films.dao.FilmDAO;
import films.helper.HeaderHelper;
import films.service.TransactionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * klasa odpowiada za pobieranie oraz przesył danych wykorzystywanych w DAO
 */
@RestController
@RequestMapping("/film")
public class FilmOperationController {

    private static final String FILM_NOT_FOUND = "Brak filmu w bazie";
    private static final String EMPTY_FIELD = "Nie wypełniono wszystkich wymaganych pól";
    private final Gson GSON = new Gson();
    private Logger logger = Logger.getLogger(FilmOperationController.class);
    @Autowired
    private FilmDAO filmDao;
    @Autowired
    private DirectorDAO directorDao;
    @Autowired
    private HeaderHelper helper;
    @Autowired
    private TransactionService transactionService;

    /**
     * metoda zbiera tytuł, po którym będzie usuwany film z bazy
     *
     * @param title - tytuł którego szukamy
     * @return ok jeżeli film usunięty
     */
    @RequestMapping(value = "/delete/{title}", method = RequestMethod.DELETE,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<String> deteleteExistingFilm(
            @PathVariable("title") String title) throws NoSuchMethodException {

        List<String> listTitle = filmDao.existingTitle();
        if (!(listTitle.contains(title))) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).headers(helper.headers()).body(FILM_NOT_FOUND);
        } else if (title.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(EMPTY_FIELD);
        }
        logger.info("Wykonuję usunięcie wpisu z bazy w klasie " + FilmOperationController.class.getSimpleName());
        transactionService.deleteService(title);
        return ResponseEntity.status(HttpStatus.OK).headers(helper.headers()).body(GSON.toJson("Usunięto film z tabeli filmów i reżyserów"));
    }

    /**
     * metoda zbiera tytuł istniejący i nowy, po którym będzie modyfikowany film w bazie
     *
     * @param oldTitle - istniejący tytuł w bazie
     * @param newTitle - nowy tytuł, na który zmieniamy
     * @return ok jeżeli operacja udana
     */
    @RequestMapping(value = "/update", method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<String> updateExistingFilm(@RequestParam("oldTitle") String oldTitle,
                                                     @RequestParam("newTitle") String newTitle) throws NoSuchMethodException {

        List<String> listTitle = filmDao.existingTitle();
        if (!(listTitle.contains(oldTitle))) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).headers(helper.headers()).body(FILM_NOT_FOUND);
        } else if (oldTitle.isEmpty() || newTitle.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(EMPTY_FIELD);
        }
        logger.info("Wykonuję aktualizację wpisu w bazie w klasie " + FilmOperationController.class.getSimpleName());
        transactionService.updateService(oldTitle, newTitle);
        return ResponseEntity.status(HttpStatus.OK).headers(helper.headers()).body(GSON.toJson("Tytuł zaktualizowany w tabeli filmów i reżyserów"));
    }

    /**
     * metoda zwraca listę wszystkich filmów bazie
     *
     * @return lista wszystkich filmów
     */
    @RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
    public String showAll() throws NoSuchMethodException {

        List<Object[]> list = filmDao.getAllFilms();
        if (list.isEmpty()) {
            ResponseEntity.status(HttpStatus.NOT_FOUND).headers(helper.headers()).body(FILM_NOT_FOUND);
        }
        return GSON.toJson(list);
    }

    /**
     * metoda zwraca listę filmów po podaniu tytułu
     *
     * @param title - tytuł, którego szukamy
     * @return lista wszystkich filmów zawierających szukany wyraz
     */
    @RequestMapping(value = "/tytul", produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, method = RequestMethod.GET)
    public String showByTitle(@RequestParam("tytul") String title) throws NoSuchMethodException, InterruptedException {

        List<String> listTitle = filmDao.existingTitle();
        List<Object[]> list = filmDao.getFilm(title);
        if (listTitle.isEmpty() || !(listTitle.contains(title))) {
            ResponseEntity.status(HttpStatus.NOT_FOUND).headers(helper.headers()).body(FILM_NOT_FOUND);
        } else if (title.isEmpty()) {
            ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(helper.headers()).body(EMPTY_FIELD);
        }
        return GSON.toJson(list);
    }
}