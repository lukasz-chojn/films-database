package films.dao;

import films.ds.DirectorDs;
import org.springframework.stereotype.Repository;

/**
 * interfejs dostępowy do bazy z reżyserami. Konfiguracja bazy zawarta jest w klasie DirectorDs
 */
@Repository
public interface DirectorDAO {
    /**
     * metoda dodaje dane reżysera do tabeli z reżyserami
     *
     * @param director
     * @throws Exception
     */
    void insert(DirectorDs director);

    /**
     * metoda usuwa wpis z tabeli na podstawie tytułu otrzymanego z kontrolera FilmOperation
     *
     * @param title - tytuł, którego szukamy w bazie
     */
    void deleteDirector(String title);

    /**
     * metoda aktualizuje wpis o reżyserze filmu w tabeli na podstawie tytułu otrzymanego z kontrolera FilmOperation
     * @param title - tytuł, dla którego chcemy zmienić reżysera
     * @param director - dane reżysera
     */
    void updateDirectorsFirstNameAndLastName(String title, String director);

    /**
     * metoda aktualizuje wpis o filmie w tabeli na podstawie tytułu otrzymanego z kontrolera FilmOperation
     * @param oldTitle - istniejący tytuł w bazie
     * @param newTitle - nowy tytuł, na który zmieniamy
     */
    void updateFilm(String oldTitle, String newTitle);
}
