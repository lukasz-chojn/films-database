package films.dao;

import films.ds.UsersDs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * intefejs z użyciem Spring data do obsługi tabeli użytkowników
 */
@Repository
public interface UsersDAO extends JpaRepository<UsersDs, Integer> {

    List<UsersDs> findByUsernameContaining(String username);

    @Query(value = "SELECT username FROM users", nativeQuery = true)
    List<String> getAllUsernames();

    @Transactional
    @Modifying
    void deleteByUsername(String username);

    @Query(value = "UPDATE users SET password = ?1 WHERE username = ?2", nativeQuery = true)
    @Transactional
    @Modifying
    void updatePassword(String newPassword, String user);

    @Query(value = "UPDATE users SET role = ?1 WHERE username = ?2", nativeQuery = true)
    @Transactional
    @Modifying
    void updateRole(String newRole, String user);
}
