package films.dao;

import films.ds.FilmDs;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * interfejs dostępowy do tabeli z filmami. Konfiguracja tabeli zawarta jest w klasie FilmDs
 */
@Repository
public interface FilmDAO {

    /**
     * metoda zwraca wszystkie wartości z tabeli
     *
     * @return lista wszystkich filmów z bazy
     */
    @SuppressWarnings("unchecked")
    List<Object[]> getAllFilms() throws NoSuchMethodException;

    /**
     * wyszukiwanie po tytule. Tytuł przekazany z kontrolera FilmOperation
     * @param title - tytuł, po którym szukamy
     * @return lista filmów zawierająca szukany wyraz
     */
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED)
    List<Object[]> getFilm(String title) throws InterruptedException;

    /**
     * metoda pobiera listę tytułów z tabeli z kolumny tytul
     * @return lista wszystkich tytułów
     */
    List<String> existingTitle() throws NoSuchMethodException;

    /**
     * metoda usuwa wpis z tabeli na podstawie tytułu otrzymanego z kontrolera FilmOperation
     * @param title - tytuł, którego szukamy
     */
    void deleteFilm(String title);

    /**
     * metoda aktualizuje podany tytuł w tabeli na nowy otrzymany z kontrolera FilmOperation
     * @param oldTitle - tytuł szukany w bazie
     * @param newTitle - tytuł, na który zmieniamy
     */
    void updateFilm(String oldTitle, String newTitle);

    /**
     * metoda zapisuje do tabeli dane otrzymane z kontrolera dataFromUser
     * @param film - obiekt otrzymany z kontrolera dataFromUser
     * @throws Exception
     */
    void insert(FilmDs film);
}
