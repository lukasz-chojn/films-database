package films.dao;

import films.ds.DirectorDs;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * klasa implementuje interfejs DAO do obsługi podstawowych operacji na bazie
 */
@Repository
public class DirectorDaoImpl implements DirectorDAO {

    private Logger logger = Logger.getLogger(DirectorDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void insert(DirectorDs director) {
        if (director == null) {
            logger.error("Obiekt nie może być pusty " + DirectorDaoImpl.class.getSimpleName());
        }
        try {
            logger.info("Wykonuję zapis do bazy z poziomu klasy " + DirectorDaoImpl.class.getSimpleName());
            entityManager.persist(director);
        } catch (Exception e) {
            logger.error("Wystąpił błąd w zapisie do tabeli z reżyserami " + DirectorDaoImpl.class.getSimpleName(), e);
        }
    }

    @Override
    public void deleteDirector(String title) {
        Query director = entityManager.createQuery("DELETE FROM DirectorDs director WHERE director.tytul = :tytul");
        try {
            logger.info("Wykonuję zapytanie " + director + " na klasie " + DirectorDaoImpl.class.getSimpleName());
            director.setParameter("tytul", title).executeUpdate();
        } catch (Exception e) {
            logger.error("Wystąpił problem przy usuwaniu wpisu z tabeli z reżyserami " + DirectorDaoImpl.class.getSimpleName(), e);
        }
    }

    @Override
    public void updateDirectorsFirstNameAndLastName(String title, String director) {
        Query updateDirector = entityManager.createQuery("UPDATE DirectorDs director SET director.imieInazwisko = :noweDane WHERE director.tytul = :tytul");
        try {
            logger.info("Wykonuję zapytanie " + updateDirector + " na klasie " + DirectorDaoImpl.class.getSimpleName());
            updateDirector.setParameter("tytul", title);
            updateDirector.setParameter("noweDane", director).executeUpdate();
        } catch (Exception e) {
            logger.error("Wystąpił problem przy aktualizacji danych reżysera w tabeli z reżyserami " + DirectorDaoImpl.class.getSimpleName(), e);
        }
    }

    @Override
    public void updateFilm(String oldTitle, String newTitle) {
        Query fkRemove = entityManager.createNativeQuery("ALTER TABLE director DROP FOREIGN KEY FK_tytul");
        Query updateFilm = entityManager.createQuery("UPDATE DirectorDs director SET director.tytul = :NowyTytul WHERE director.tytul = :tytul");
        try {
            logger.info("Wykonuję zapytanie " + fkRemove + "oraz " + updateFilm + " na klasie " + DirectorDaoImpl.class.getSimpleName());
            fkRemove.executeUpdate();
            updateFilm.setParameter("tytul", oldTitle);
            updateFilm.setParameter("NowyTytul", newTitle).executeUpdate();
        } catch (Exception e) {
            logger.error("Wystąpił problem przy aktualizacji tytułu filmu w tabeli z reżyserami " + DirectorDaoImpl.class.getSimpleName(), e);
        }
    }
}
