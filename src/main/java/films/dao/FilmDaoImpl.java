package films.dao;

import films.ds.DirectorDs;
import films.ds.FilmDs;
import org.apache.log4j.Logger;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * klasa implementuje interfejs DAO do obsługi podstawowych operacji na bazie z filmami
 */
@Repository
public class FilmDaoImpl implements FilmDAO {

    private Logger logger = Logger.getLogger(FilmDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @SuppressWarnings("unchecked")
    public List<Object[]> getAllFilms() throws NoSuchMethodException {
        logger.info("Wykonuję zapytanie na klasie " + this.getClass().getSimpleName() + " w metodzie " + FilmDaoImpl.class.getMethod("getAllFilms"));
        return entityManager.createQuery("SELECT film.id, film.tytul, film.czasTrwania, film.rozmiar, director.imieInazwisko FROM FilmDs film " +
                "JOIN DirectorDs director ON film.tytul=director.tytul").getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<String> existingTitle() throws NoSuchMethodException {
        logger.info("Wykonuję zapytanie na klasie " + this.getClass().getSimpleName() + " w metodzie " + FilmDaoImpl.class.getMethod("existingTitle"));
        return entityManager.createQuery("SELECT film.tytul FROM FilmDs film").getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Object[]> getFilm(String title) throws InterruptedException {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
        fullTextEntityManager.createIndexer(DirectorDs.class).purgeAllOnStart(true).optimizeOnFinish(true).startAndWait();
        QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(DirectorDs.class).get();
        org.apache.lucene.search.Query query = queryBuilder.keyword().wildcard().onField("tytul").matching("*" + title.toLowerCase() + "*").createQuery();
        return fullTextEntityManager.createFullTextQuery(query, DirectorDs.class).getResultList();
    }

    @Override
    public void deleteFilm(String title) {
        Query film = entityManager.createQuery("DELETE FROM FilmDs film WHERE film.tytul = :tytul");
        try {
            logger.info("Wykonuję zapytanie " + film + " na klasie " + FilmDaoImpl.class.getSimpleName());
            film.setParameter("tytul", title).executeUpdate();
        } catch (Exception e) {
            logger.error("Wystąpił błąd przy usuwaniu filmu z tabeli tytułów " + FilmDaoImpl.class.getSimpleName(), e);
        }
    }

    @Override
    public void updateFilm(String oldTitle, String newTitle) {
        Query fkAdd = entityManager.createNativeQuery("ALTER TABLE director ADD CONSTRAINT FK_tytul FOREIGN KEY (tytul) REFERENCES film(tytul)");
        Query query = entityManager.createQuery("UPDATE FilmDs film SET film.tytul = :NowyTytul WHERE film.tytul = :tytul");
        try {
            logger.info("Wykonuję zapytanie " + fkAdd + " oraz " + query + " na klasie " + this.getClass().getSimpleName());
            query.setParameter("tytul", oldTitle);
            query.setParameter("NowyTytul", newTitle).executeUpdate();
            fkAdd.executeUpdate();
        } catch (Exception e) {
            logger.error("Wystąpił problem przy aktualizacji tytułu filmu w tabeli tytułów. " + FilmDaoImpl.class.getSimpleName(), e);
        }
    }

    @Override
    public void insert(FilmDs film) {
        if (film == null) {
            logger.error("Obiekt nie może być pusty " + FilmDaoImpl.class.getSimpleName());
        }
        try {
            entityManager.persist((film));
        } catch (Exception e) {
            logger.error("Wystąpił błąd zapisu filmu do tabeli z tytułami filmów " + FilmDaoImpl.class.getSimpleName(), e);
        }
    }
}
