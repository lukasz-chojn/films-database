package films.helper;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa opakowywująca listę błędów w stringa
 */
@Component
public class MessageBuilderHelper {

    private List<String> errorList = new ArrayList<>();

    public MessageBuilderHelper() {
    }

    public MessageBuilderHelper(List<String> errorList) {
        this.errorList = errorList;
    }

    public List<String> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<String> errorList) {
        this.errorList = errorList;
    }

    @Override
    public String toString() {
        return errorList.toString();
    }
}
