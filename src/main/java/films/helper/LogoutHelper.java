package films.helper;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * klasa pomocnicza odpowiadająca za obsługę wylogowania
 */
@Component
public class LogoutHelper extends SimpleUrlLogoutSuccessHandler implements LogoutSuccessHandler {

    private Logger logger = Logger.getLogger(LogoutHelper.class);

    @Override
    public void onLogoutSuccess(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) throws IOException, ServletException {
        response.sendRedirect("./index.html");
        super.onLogoutSuccess(request, response, authentication);
        logger.info("Wylogowanie z użyciem klasy " + LogoutHelper.class.getSimpleName());
    }
}
