package films.helper;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

/**
 * klasa pomocnicza odpowiadająca za obsługę nagłówków http
 */
@Component
public class HeaderHelper {

    private Logger logger = Logger.getLogger(HeaderHelper.class);

    public HttpHeaders headers() {
        logger.info("Wywołano użycie klasy " + HeaderHelper.class.getSimpleName());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "text/html; charset=UTF-8");
        return headers;
    }
}
