package films.helper;

import org.apache.log4j.Logger;

/**
 * klasa pomocnicza posiadająca listę ról w systemie
 */
public enum UserRole {
    ADMIN("admin"),
    USER("user");

    private Logger logger = Logger.getLogger(UserRole.class);

    private String role;

    UserRole(String role) {
        logger.info("Wywołano użycie klasy " + UserRole.class.getSimpleName());
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}