package films.helper;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Klasa weryfikująca liczbę użytkowników w roli admin
 */
@Component
public class SearchAdminHelper {

    private Logger logger = Logger.getLogger(SearchAdminHelper.class);

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public List<String> checkExistingAdmins() {
        logger.info("Wykonuję zapytanie do bazy w klasie " + SearchAdminHelper.class.getSimpleName());
        return entityManager.createQuery("SELECT u.username FROM UsersDs u WHERE role = 'admin'").getResultList();
    }
}
