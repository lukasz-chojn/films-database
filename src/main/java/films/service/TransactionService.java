package films.service;

import films.dao.DirectorDAO;
import films.dao.FilmDAO;
import films.ds.DirectorDs;
import films.ds.FilmDs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Klasa integrująca metody z interfejsów i opakowująca je w transakcję
 */
@Service
public class TransactionService {

    @Autowired
    private FilmDAO filmDao;
    @Autowired
    private DirectorDAO directorDao;

    @Transactional(propagation = Propagation.REQUIRED)
    public void insertService(FilmDs film, DirectorDs director) {
        filmDao.insert(film);
        directorDao.insert(director);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteService(String title) {
        directorDao.deleteDirector(title);
        filmDao.deleteFilm(title);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateService(String oldTitle, String newTitle) {
        directorDao.updateFilm(oldTitle, newTitle);
        filmDao.updateFilm(oldTitle, newTitle);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void directorUpdateService(String title, String director) {
        directorDao.updateDirectorsFirstNameAndLastName(title, director);
    }
}
