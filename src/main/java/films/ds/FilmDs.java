package films.ds;

import org.hibernate.search.annotations.Field;

import javax.persistence.*;
import java.io.Serializable;

/**
 * konfiguracja tabeli z filmami
 */
@Entity
@Table(name = "film")
@Embeddable
public class FilmDs implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;
    @Column(name = "tytul", length = 255, nullable = false)
    private String tytul;
    @Column(name = "czasTrwania", length = 255, nullable = false)
    @Field
    private String czasTrwania;
    @Column(name = "rozmiar", precision = 2, scale = 6)
    @Field
    private double rozmiar;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public String getCzasTrwania() {
        return czasTrwania;
    }

    public void setCzasTrwania(String czasTrwania) {
        this.czasTrwania = czasTrwania;
    }

    public double getRozmiar() {
        return rozmiar;
    }

    public void setRozmiar(double rozmiar) {
        this.rozmiar = rozmiar;
    }
}
