package films.ds;

import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.WhitespaceTokenizerFactory;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilterFactory;
import org.apache.lucene.analysis.ngram.EdgeNGramFilterFactory;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.*;

import javax.persistence.*;

/**
 * konfiguracja tabeli z tytułem filmu i reżyserem
 */
@Entity
@Table(name = "director")
@AnalyzerDef(name = "titles",
        tokenizer = @TokenizerDef(factory = WhitespaceTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = ASCIIFoldingFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(
                        factory = EdgeNGramFilterFactory.class,
                        params = {
                                @Parameter(name = "minGramSize", value = "1"),
                                @Parameter(name = "maxGramSize", value = "10000")
                        }
                )
        })
@AnalyzerDef(name = "titles_query",
        tokenizer = @TokenizerDef(factory = WhitespaceTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = ASCIIFoldingFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class)
        })
@Indexed
public class DirectorDs {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;
    @Column(name = "imieInazwisko", length = 255, nullable = false)
    @Field
    private String imieInazwisko;
    @OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    @JoinColumn(name = "tytul", referencedColumnName = "tytul", foreignKey = @ForeignKey(name = "FK_tytul"))
    @IndexedEmbedded
    private FilmDs film;
    @Column(insertable = false, updatable = false, length = 255, nullable = false)
    @Field
    private String tytul;

    public FilmDs getFilm() {
        return film;
    }

    public void setFilm(FilmDs film) {
        this.film = film;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImieInazwisko() {
        return imieInazwisko;
    }

    public void setImieInazwisko(String imieInazwisko) {
        this.imieInazwisko = imieInazwisko;
    }
}
